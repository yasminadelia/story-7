from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def index(request):
	context = {
		'page_title': 'Home',
	}

	template_name = None

	if request.user.is_authenticated:
		#untuk user
		template_name = 'home.html'

	else:
		#untuk anon
		template_name = 'home-anon.html'

	return render(request, template_name, context)

def loginView(request):
	context = {
		'page_title' : 'Login',
	}

	user = None

	# if request.method == "GET":
	# 	if request.user.is_authenticated:
	# 		return redirect('index')

	# 	else:
	# 		return render(request, 'login.html', context)

	if request.method == "POST":
		username_login = request.POST['username']
		password_login = request.POST['password']

		user = authenticate(request, username=username_login, password=password_login)

		if user is not None:
			login(request, user)
			return redirect('index')
		else:
			return redirect('login')
	return render(request, 'login.html', context)
		
		
# @login_required
def logoutView(request):
	context = {
		'page_title' : 'Logout',
	}


	if request.method == "POST":
		if request.POST['logout'] == "Yes":
			logout(request)
		

		return redirect('index')


	return render(request, 'logout.html', context)