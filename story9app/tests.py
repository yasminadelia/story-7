from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .views import *

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class UnitTest(TestCase):
	def test_home_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_home_using_right_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'home-anon.html')
	def test_home_using_right_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'home-anon.html')

	def test_url_login_exist(self):
		response = Client().get("/login/")
		self.assertEqual(response.status_code, 200)

	def test_login_used_right_template(self):
		response = Client().get('/login/')
		self.assertTemplateUsed(response, 'login.html')

	def test_url_logout_exist(self):
		response = Client().get("/logout/")
		self.assertEqual(response.status_code, 200)

	def test_logout_used_right_template(self):
		response = Client().get('/logout/')
		self.assertTemplateUsed(response, 'logout.html')

	def test_page_is_not_exist(self):
		self.response = Client().get('/NotExistPage/')
		self.assertEqual(self.response.status_code, 404)


class Story9AppFunctionalTest(StaticLiveServerTestCase):

	def setUp(self):
		
		chrome_options = Options()
		chrome_options.add_argument("--disable-dev-shm-usage")
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')


		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story9AppFunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story9AppFunctionalTest, self).tearDown()

	def test_login_logout(self):
		selenium = self.selenium
        # Opening the link we want to test
		selenium.get('http://localhost:8000/')
		time.sleep(5) 

		
		time.sleep(1)
		selenium.find_element_by_id("login-but").click()
		

		username_input = selenium.find_element_by_id('username')
		username_input.send_keys('bambang')
		password_input = selenium.find_element_by_id('password')
		password_input.send_keys('tes123456')

		selenium.find_element_by_id('submit-but').click()

		
		time.sleep(2)
		selenium.find_element_by_id("logout-but").click()
		
		
		selenium.find_element_by_id("logout-yes").click()
		
