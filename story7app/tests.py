from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .views import *

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTest(TestCase):
	
	def test_home_url_exist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code, 200)

	def test_home_using_right_template(self):
		response = Client().get('/profile/')
		self.assertTemplateUsed(response, 'story7app.html')

	def test_calling_right_views_function(self):
		found = resolve('/profile/')
		self.assertEqual(found.func, views.profile)

	def test_page_is_not_exist(self):
		self.response = Client().get('wek/')
		self.assertEqual(self.response.status_code, 404)

# class Story7AppFunctionalTest(StaticLiveServerTestCase):

# 	def setUp(self):
		
# 		chrome_options = Options()
# 		chrome_options.add_argument("start-maximized");
# 		chrome_options.add_argument("--disable-dev-shm-usage")
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')        
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')
# 		chrome_options.add_argument("disable-infobars")
# 		chrome_options.add_argument("--disable-extensions")


# 		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 		super(Story7AppFunctionalTest, self).setUp()

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(Story7AppFunctionalTest, self).tearDown()

# 	def test_change_color_button(self):
# 		selenium = self.selenium
#         # Opening the link we want to test
# 		selenium.get('http://localhost:8000/profile/')
# 		time.sleep(5)

		 

# 		color = self.selenium.find_element_by_tag_name('body').value_of_css_property("color")
# 		self.assertEqual("rgba(0, 0, 0, 1)", color)

# 		time.sleep(5)
# 		selenium.find_element_by_id('light').click()
# 		time.sleep(1)
# 		color = selenium.find_element_by_tag_name('body').value_of_css_property("color")
# 		self.assertEqual("rgba(0, 0, 0, 1)", color)

# 		time.sleep(5)
# 		selenium.find_element_by_id('dark').click()
# 		time.sleep(1)
# 		color = selenium.find_element_by_tag_name('body').value_of_css_property("color")
# 		self.assertEqual("rgba(255, 255, 255, 1)", color)

# 		time.sleep(1)
# 		content = selenium.find_element_by_class_name("inside")
# 		display = content.value_of_css_property("display")
# 		self.assertEqual("none", display)

# 		time.sleep(1)
# 		selenium.find_element_by_class_name("panels").click()
# 		time.sleep(1)
# 		content = selenium.find_element_by_class_name("inside")
# 		display = content.value_of_css_property("display")
# 		self.assertEqual("block", display)

# 		time.sleep(1)
# 		selenium.find_element_by_class_name("panels").click()
# 		time.sleep(1)
# 		content = selenium.find_element_by_class_name("inside")
# 		display = content.value_of_css_property("display")
# 		self.assertEqual("none", display)



